<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerGroup extends Model
{
    protected $fillable = ['name', 'team_id'];
}
