<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClubAdmin extends Model
{
    protected $fillable = ['name', 'club_id'];

    public function club()
    {
    	return $this->belongsTo('App\Club');
    }
}
