<?php

namespace App\Listeners;

use App\Events\UserCredentails;
use App\Notifications\UserCredentailsMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserCredentailsEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCredentails  $event
     * @return void
     */
    public function handle(UserCredentails $event)
    {
        $event->user->notify(new UserCredentailsMail($event->user, $event->token));  
    }
}
