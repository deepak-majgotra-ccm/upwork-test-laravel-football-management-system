<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
	
	Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as'=>'admin.'], function(){
		
		Route::resource('club', 'ClubController');

		Route::resource('user', 'User\UserController');

		Route::resource('team', 'TeamController');

		Route::resource('player', 'PlayerController');

		Route::resource('player-group', 'PlayerGroupController');

		Route::get('club-admin', 'ClubAdminController@index')->name('club.admin.index');
		
		Route::get('club/{club}/admin', 'ClubAdminController@create')->name('club.admin.create');

		Route::get('club-admin/{clubAdmin}', 'ClubAdminController@edit')->name('club.admin.edit');

		Route::post('club-admin/{clubAdmin}', 'ClubAdminController@update')->name('club.admin.update');

		Route::delete('club-admin/{clubAdmin}', 'ClubAdminController@destroy')->name('club.admin.destroy');
		
		Route::post('club/{club}/admin', 'ClubAdminController@store')->name('club.admin.store');
	});

});
