## Installation

Clone the repository

git clone git@bitbucket.org:deepak-majgotra-ccm/upwork-test-laravel-football-management-system.git

Switch to the repo folder

cd laravel-football-club/code

## Permissions

Run these two commands to give permission to storage folder. 

1. sudo chgrp -R www-data storage bootstrap/cache
2. sudo chmod -R ug+rwx storage bootstrap/cache


Install all the dependencies using composer

composer install

Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env

Generate a new application key

php artisan key:generate

Run the database migrations (Set the database connection in .env before migrating)

php artisan migrate

Start the local development server

php artisan serve

You can now access the server at http://localhost:8000

OR 

Using http://localhost/laravel-football-club/code/public/

TL;DR command list

git clone git@bitbucket.org:deepak-majgotra-ccm/upwork-test-laravel-football-management-system.git
composer install
cp .env.example .env
php artisan key:generate

Make sure you set the correct database connection information before running the migrations Environment variables

php artisan migrate
php artisan serve

## Database seeding

Populate the database with seed data with relationships which includes users. This can help you to quickly start testing the dashboard.

Run the database seeder

php artisan db:seed

Note : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

php artisan migrate:refresh

## Login Credentials For Super Admin

User Name: admin@footballclub.com

Password: 123456