@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
        <a href="{{route('admin.club.create')}}" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new club</a>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Club Admin</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($clubs as $club)
        <tr>
            <td>{{$club->id}}</td>
            <td>{{$club->name}}</td>
            @if($club->admin)
            <td>{{$club->admin->name}}</td>
            @else
            <td><a class='btn btn-info btn-xs' href="{{route('admin.club.admin.create', $club->id)}}"><span class="glyphicon glyphicon-plus"></span>Add Admin</a></td>
            @endif
            <td class="text-center"><a class='btn btn-info btn-xs' href="{{route('admin.club.edit', $club->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{route('admin.club.destroy', [$club->id])}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
        </tr>
        @endforeach
    </table>
@endsection