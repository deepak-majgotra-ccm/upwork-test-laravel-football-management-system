@extends('admin.layouts.dashboard')
@section('content')
    <h2>Edit</h2>
    <form action="{{route('admin.club.update', $club->id)}}" method="post">
    	@method('PATCH')
    	@include('admin.club._fields')
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Update" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection