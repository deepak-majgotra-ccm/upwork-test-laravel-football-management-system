@extends('admin.layouts.dashboard')
@section('content')
    <h2>Player Group</h2>
    <form action="{{route('admin.player.store')}}" method="post">
    	@csrf
    	<div class="row">
			<div class="col-md-6">
			    <label>Name</label>
			    <input type="text" name="name">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			    <label>Player Group Name</label>
			    <select name="player_group_id">
			    	<option value="">Please Select Player Group</option>
			    	@foreach($playerGroups as $playerGroup)
			    		<option value="{{$playerGroup->id}}">{{$playerGroup->name}}</option>
			    	@endforeach
				</select>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
			    <label>Photo</label>
			    <input type="file" name="photo">
			</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Create" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection