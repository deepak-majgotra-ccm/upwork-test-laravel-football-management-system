@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
            <a href="{{route('admin.user.create')}}" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new user</a>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td class="text-center"><a class='btn btn-info btn-xs' href="{{route('admin.club.admin.edit', $user->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{route('admin.club.admin.destroy', $user->id)}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
        </tr>
        @endforeach
    </table>
@endsection