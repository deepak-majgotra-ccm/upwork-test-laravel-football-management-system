@extends('admin.layouts.dashboard')
@section('content')
    <h2>Club Admin</h2>
    <form action="{{route('admin.club.admin.update', $clubAdmin->id)}}" method="post">
    	@csrf
    	<div class="row">
			<div class="col-md-6">
			    <label>Name</label>
			    <input type="text" name="name" value="{{$clubAdmin->name}}">
			</div>
		</div>
		<div class="row">
		    <div class="col-md-6">
		    	<input type="submit" value="Update" class="btn btn-primary pull-right">
			</div>
		</div>
    </form>
@endsection