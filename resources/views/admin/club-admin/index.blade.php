@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Club</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($clubAdmin as $admin)
        <tr>
            <td>{{$admin->id}}</td>
            <td>{{$admin->name}}</td>
            <td>{{$admin->club->name}}</td>
            <td class="text-center"><a class='btn btn-info btn-xs' href="{{route('admin.club.admin.edit', $admin->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{route('admin.club.admin.destroy', [$admin->id])}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
        </tr>
        @endforeach
    </table>
@endsection