@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
        <a href="{{route('admin.player-group.create')}}" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new player group</a>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Team</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($playerGroups as $playerGroup)
        <tr>
            <td>{{$playerGroup->id}}</td>
            <td>{{$playerGroup->name}}</td>
            <td>{{$playerGroup->name}}</td>
            <td class="text-center"><a class='btn btn-info btn-xs' href="{{route('admin.player-group.edit', $playerGroup->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> <a href="{{route('admin.player-group.destroy', $playerGroup->id)}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a></td>
        </tr>
        @endforeach
    </table>
@endsection