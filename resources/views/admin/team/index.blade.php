@extends('admin.layouts.dashboard')
@section('content')
    <table class="table table-striped custab">
        <thead>
        <a href="{{route('admin.team.create')}}" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new team</a>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        @foreach($teams as $team)
        <tr>
            <td>{{$team->id}}</td>
            <td>{{$team->name}}</td>
            <td class="text-center">
                <a class='btn btn-info btn-xs' href="{{route('admin.team.edit', $team->id)}}"><span class="glyphicon glyphicon-edit"></span> Edit</a> 
                <a href="{{route('admin.team.destroy', $team->id)}}" data-method="delete" data-token="{{csrf_token()}}" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure?')"><span class="glyphicon glyphicon-remove"></span> Del</a>
            </td>
        </tr>
        @endforeach
    </table>
@endsection